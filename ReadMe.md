secpwgen.py [![Unlicensed work](https://raw.githubusercontent.com/unlicense/unlicense.org/master/static/favicon.png)](https://unlicense.org/)
===============
[![TravisCI Build Status](https://travis-ci.org/KOLANICH/secpwgen.py.svg?branch=master)](https://travis-ci.org/KOLANICH/secpwgen.py)
[![Coveralls Coverage](https://img.shields.io/coveralls/KOLANICH/secpwgen.py.svg)](https://coveralls.io/r/KOLANICH/secpwgen.py)
[![Libraries.io Status](https://img.shields.io/librariesio/github/KOLANICH/secpwgen.py.svg)](https://libraries.io/github/KOLANICH/secpwgen.py)


A pure python dropin replacement for [secpwgen](https://linux.die.net/man/1/secpwgen).  Some features are added like QR Code generation.

In order to this work on python<3.6 you need to manually install the [secrets library](https://raw.githubusercontent.com/python/cpython/master/Lib/secrets.py) by downloading it and putting into the dir where default python packages reside.
